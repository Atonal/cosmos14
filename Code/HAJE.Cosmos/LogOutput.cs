﻿using HAJE.Andromeda.GameBase;
using HAJE.Andromeda.GameBase.Graphics;
using System.Collections.Generic;

namespace HAJE.Cosmos
{
    public class LogOutput : ITextOutput
    {
        public LogOutput(Font font)
        {
            const int left = 5;
            const int top = 5;
            const int lineHeight = 15;

            labels = new Label[50];
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i] = new Label();
                labels[i].X = left;
                labels[i].Y = top + i * lineHeight;
                labels[i].Font = font;
                labels[i].Color = Color.Yellow;
            }
        }

        public void WriteLine(string text)
        {
            logList.Enqueue(text);
            while (logList.Count > labels.Length)
                logList.Dequeue();
            int i = 0;
            foreach (string s in logList)
                labels[i++].Text = s;
        }

        public void Draw()
        {
            for (int i = 0; i < labels.Length; i++)
                labels[i].Draw();
        }

        private Queue<string> logList = new Queue<string>();
        private Label[] labels;
    }
}
