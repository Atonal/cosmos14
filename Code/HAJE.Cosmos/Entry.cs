﻿using HAJE.Andromeda.GameBase;
using HAJE.Andromeda.GameBase.Graphics;
using HAJE.Andromeda.GameBase.Inputs;
using HAJE.Cosmos.Network.Local;
using System;
using System.Collections.Generic;

namespace HAJE.Cosmos
{
    class Entry
    {
        [STAThread]
        public static void Main(string[] args)
        {
            // 게임 기반 시스템 초기화
            ErrorReporter.BeginReport();
            GameForm form = new GameForm(1290, 768);
            form.Title = "Cosmos Online";
            Device graphicsDevice = new Device(form);
            Mouse mouse = new Mouse(form);
            Keyboard keyboard = new Keyboard(form);
            GameTime gameTime = new GameTime();
            UpdateDispatcher updateLoop = new UpdateDispatcher();
            
            // 서버/클라이언트 초기화
            FontFactory factory = new FontFactory(graphicsDevice);
            factory.Name = "Consolas";
            factory.IncludeKorean = false;
            factory.Height = 14;
            Font font = factory.Create();
            LogOutput logOutput = new LogOutput(font);
            Network.Local.Server server = new Network.Local.Server();
            server.Start();
            updateLoop.Register(server);
            Network.Local.Client client = new Network.Local.Client();
            client.Connect("localhost");
            updateLoop.Register(client);

            // 입력 동기화 관련 초기화
            int playerCount = 1;
            Network.Local.InputSender inputSender = new Network.Local.InputSender(client);
            Network.Local.InputDispatcher inputDispatcher = new Network.Local.InputDispatcher(server, playerCount);
            Network.Local.InputReceiver inputReceiver = new Network.Local.InputReceiver(client, playerCount);
            PlayerInputState inputState = new PlayerInputState();
            server.OnConnectionChanged += () => {
                inputSender.Begin(keyboard);
                inputDispatcher.Begin();
                inputReceiver.Begin();
            };

            // 배경 테스트
            Background.BackgroundRenderer background = new Background.BackgroundRenderer(graphicsDevice);
            Background.SimpleStarBuilder bgBuilder = new Background.SimpleStarBuilder();
            bgBuilder.AddPalette(new Background.ColorPalette()
            {
                Brightness = new Background.FloatRange(0.5f, 1),
                ColorFrom = Background.StarColor.Red,
                ColorTo = Background.StarColor.Orange,
                ToBlend = new Background.FloatRange(0, 1),
            }, 1);
            bgBuilder.AddUniformRectangular(-1000, 1000, -1000, 1000, 750, 1000, 10.0f);
            background.SetDotStarData(graphicsDevice, bgBuilder.FlushData());

            background.SetQuadStarData(graphicsDevice, new Background.SimpleStarData[]{
                new Background.SimpleStarData()
            {
                Position = new Vector3(0, 0, 100),
                Color = Background.StarColor.Red
            }});

            Camera camera = new Camera(graphicsDevice.Viewport);

            mouse.Visible = false;
            form.BeginLoop(() => {
                gameTime.Refresh();
                updateLoop.Update(gameTime);

                Network.Local.FrameInput input;
                while ((input = inputReceiver.FetchInput()) != null)
                {
                    inputState.ClearFrame();
                    List<InputCommand> list = input.Inputs[0].CommandList;
                    foreach (InputCommand cmd in list)
                    {
                        logOutput.WriteLine(cmd.ToString());
                        inputState.ApplyCommand(cmd);
                    }

                    inputReceiver.NextInput();
                }

                // 위치 이동
                float deltaPos = 100 * gameTime.DeltaTime;
                if (inputState.IsKeyPressed(Keys.D))
                {
                    camera.SetPosition(camera.Position + new Vector2(deltaPos, 0));
                }
                if (inputState.IsKeyPressed(Keys.A))
                {
                    camera.SetPosition(camera.Position + new Vector2(-deltaPos, 0));
                }
                if (inputState.IsKeyPressed(Keys.W))
                {
                    camera.SetPosition(camera.Position + new Vector2(0, deltaPos));
                }
                if (inputState.IsKeyPressed(Keys.S))
                {
                    camera.SetPosition(camera.Position + new Vector2(0, -deltaPos));
                }

                graphicsDevice.BeginScene();
                graphicsDevice.Clear(Color.Black);
                graphicsDevice.Transform = camera.TransformMatrix;
                background.Render(graphicsDevice);
                logOutput.Draw();
                graphicsDevice.EndScene();
                graphicsDevice.Present(); 
            });

            background.Dispose();
            font.Dispose();
            graphicsDevice.Dispose();
            form.Dispose();
        }
    }
}
