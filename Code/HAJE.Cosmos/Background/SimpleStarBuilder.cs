﻿using HAJE.Andromeda.GameBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace HAJE.Cosmos.Background
{
    public struct FloatRange
    {
        public FloatRange(float min, float max)
        {
            Debug.Assert(min <= max);
            this.min = min;
            this.max = max;
        }

        public void Set(float min, float max)
        {
            Debug.Assert(min <= max);
            this.min = min;
            this.max = max;
        }

        public float Min
        {
            get
            {
                return min;
            }
            set
            {
                Debug.Assert(value <= max);
                min = value;
            }
        }
        public float Max
        {
            get
            {
                return max;
            }
            set
            {
                Debug.Assert(min <= value);
                max = value;
            }
        }

        private float min;
        private float max;
    }

    public struct ColorPalette
    {
        public Color ColorFrom;
        public Color ColorTo;
        public FloatRange ToBlend;    // 0에서 1사이
        public FloatRange Brightness;   // 0에서 1사이
    }

    public class SimpleStarBuilder
    {
        public void AddPalette(ColorPalette palette, int weight)
        {
            Debug.Assert(weight > 0);
            palettes.Add(new RegisteredPalette(palette, weight));
            weightSum += weight;
            partialSums.Add(weightSum);
        }

        public void ClearPalette()
        {
            partialSums.Clear();
            palettes.Clear();
            weightSum = 0;
        }

        public void AddUniformRectangular(float minX, float maxX, float minY, float maxY, float minZ, float maxZ, float density)
        {
            Debug.Assert(weightSum > 0);

            float area = (maxX - minX) * (maxY - minY);
            int count = (int)(area * density / 10000);
            count = Math.Max(count, 1);

            for (int i = 0; i < count; i++)
            {
                SimpleStarData s;
                s.Position = new Vector3(randFloat(minX, maxX), randFloat(minY, maxY), randFloat(minZ, maxZ));

                int w = rand.Next(weightSum);
                int paletteIndex = 0;
                for (int j = 0; j < partialSums.Count; j++)
                {
                    if (w < partialSums[j])
                    {
                        paletteIndex = j;
                        break;
                    }
                }

                var palette = palettes[paletteIndex].Palette;
                float fromRatio = randFloat(palette.ToBlend.Min, palette.ToBlend.Max);
                Color color = Color.RGBLerp(palette.ColorFrom, palette.ColorTo, fromRatio);
                float brightness = randFloat(palette.Brightness.Min, palette.Brightness.Max);
                s.Color = color * brightness;
                starData.Add(s);
            }
        }

        public SimpleStarData[] FlushData()
        {
            var ret = starData.ToArray();
            starData.Clear();
            return ret;
        }

        public void InitSeed(int seed)
        {
            rand = new Random(seed);
        }

        #region privates

        float randFloat(float min, float max)
        {
            return (float)(rand.NextDouble() * (max - min) + min);
        }

        struct RegisteredPalette
        {
            public RegisteredPalette(ColorPalette palette, int weight)
            {
                this.Palette = palette;
                this.Weight = weight;
            }

            public ColorPalette Palette;
            public int Weight;
        }
        List<RegisteredPalette> palettes = new List<RegisteredPalette>();
        List<int> partialSums = new List<int>();
        List<SimpleStarData> starData = new List<SimpleStarData>();
        int weightSum = 0;
        Random rand = new Random();

        #endregion
    }
}
