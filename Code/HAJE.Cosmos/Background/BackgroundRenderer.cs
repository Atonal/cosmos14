﻿using HAJE.Andromeda.GameBase;
using HAJE.Andromeda.GameBase.Graphics;
using System;

namespace HAJE.Cosmos.Background
{
    public struct SimpleStarData
    {
        public Vector3 Position;
        public Color Color;
    }

    public class BackgroundRenderer : IDisposable
    {
        public BackgroundRenderer(Device device)
        {
        }

        public void SetDotStarData(Device device, SimpleStarData[] dotStars)
        {
            if (dotVertices != null)
                dotVertices.Dispose();

            dotVertices = new VertexBuffer<VertexType.PositionColor>(device, dotStars.Length);
            dotVertices.BeginWrite();
            for (int i = 0; i < dotStars.Length; i++)
                dotVertices.Write(new VertexType.PositionColor()
                {
                    Color = dotStars[i].Color,
                    Position = dotStars[i].Position
                });
            dotVertices.EndWrite();

            dotDrawDescriptor = new DrawDescriptor();
            dotDrawDescriptor.SetVertexBuffer<VertexType.PositionColor>(dotVertices);
            dotDrawDescriptor.Type = PrimitiveType.Point;
        }

        public void SetQuadStarData(Device device, SimpleStarData[] quadStars)
        {
            if (quadVertices != null)
                quadVertices.Dispose();

            quadVertices = new VertexBuffer<VertexType.PositionColorTexture>(device, quadStars.Length * 4);
            quadVertices.BeginWrite();
            for (int i = 0; i < quadStars.Length; i++)
            {
                quadVertices.Write(new VertexType.PositionColorTexture()
                {
                    Color = quadStars[i].Color,
                    Position = quadStars[i].Position,
                    Texture = new Vector2(0, 0)
                });
                quadVertices.Write(new VertexType.PositionColorTexture()
                {
                    Color = quadStars[i].Color,
                    Position = quadStars[i].Position + new Vector3(0, 1, 0),
                    Texture = new Vector2(1, 0)
                });
                quadVertices.Write(new VertexType.PositionColorTexture()
                {
                    Color = quadStars[i].Color,
                    Position = quadStars[i].Position + new Vector3(1, 1, 0),
                    Texture = new Vector2(1, 1)
                });
                quadVertices.Write(new VertexType.PositionColorTexture()
                {
                    Color = quadStars[i].Color,
                    Position = quadStars[i].Position + new Vector3(1, 0, 0),
                    Texture = new Vector2(0, 1)
                });
            }
            quadVertices.EndWrite();

            if (quadIndices != null)
                quadIndices.Dispose();

            quadIndices = new IndexBuffer(device, quadStars.Length * 6);
            quadIndices.BeginWrite();
            for (int i = 0; i < quadStars.Length; i++)
            {
                int baseIndex = i * 6;
                quadIndices.Write(baseIndex + 0);
                quadIndices.Write(baseIndex + 1);
                quadIndices.Write(baseIndex + 2);
                quadIndices.Write(baseIndex + 2);
                quadIndices.Write(baseIndex + 3);
                quadIndices.Write(baseIndex + 0);
            }
            quadIndices.EndWrite();

            quadDrawDescriptor = new DrawDescriptor();
            quadDrawDescriptor.SetVertexBuffer<VertexType.PositionColorTexture>(quadVertices);
            quadDrawDescriptor.SetIndexBuffer(quadIndices);
            quadDrawDescriptor.Type = PrimitiveType.Triangle;
        }

        public void Render(Device device)
        {
            device.RenderState.Lighting = false;
            device.RenderState.ZEnable = false;
            device.RenderState.AlphaBlendEnable = true;
            device.RenderState.SetAdditiveBlending();
            device.Draw(dotDrawDescriptor);
            device.Draw(quadDrawDescriptor);
        }

        public void Dispose()
        {
            if (dotVertices != null)
                dotVertices.Dispose();
            dotVertices = null;
        }

        VertexBuffer<VertexType.PositionColor> dotVertices;
        DrawDescriptor dotDrawDescriptor;

        VertexBuffer<VertexType.PositionColorTexture> quadVertices;
        IndexBuffer quadIndices;
        DrawDescriptor quadDrawDescriptor;
    }
}
