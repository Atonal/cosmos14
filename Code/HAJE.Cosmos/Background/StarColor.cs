﻿using HAJE.Andromeda.GameBase;

namespace HAJE.Cosmos.Background
{
    public static class StarColor
    {
        public readonly static Color DarkRed = Color.FromFloat(0.5f, 0, 0);
        public readonly static Color Red = Color.FromFloat(1.0f, 0, 0);
        public readonly static Color Orange = Color.FromFloat(1.0f, 0.5f, 0);
    }
}
