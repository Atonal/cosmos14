﻿
namespace HAJE.Cosmos
{
    public interface ITextOutput
    {
        void WriteLine(string text);
    }
}
