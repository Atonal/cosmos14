﻿using System;
using System.Collections.Generic;

namespace HAJE.Cosmos
{
    public class DisposePool : IDisposable
    {
        public void Add(IDisposable d)
        {
            disposables.Add(d);
        }

        public void Remove(IDisposable d)
        {
            disposables.Remove(d);
        }

        public void Dispose()
        {
            foreach (var d in disposables)
                d.Dispose();
            disposables.Clear();
        }

        List<IDisposable> disposables = new List<IDisposable>();
    }
}
