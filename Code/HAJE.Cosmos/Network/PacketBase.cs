﻿using Lidgren.Network;
using System;
using System.Diagnostics;

namespace HAJE.Cosmos.Network
{
    public abstract class PacketBase
    {
        public PacketBase(int code)
        {
            this.code = code;
            this.DeliveryMethod = NetDeliveryMethod.ReliableOrdered;
        }

        public int Code
        {
            get
            {
                return code;
            }
        }
        public NetDeliveryMethod DeliveryMethod
        {
            get;
            protected set;
        }
        public int SequenceChannel
        {
            get;
            protected set;
        }

        public event Action OnReceived = delegate { };

        #region read/write message

        public void WriteMessage(NetOutgoingMessage msg)
        {
            msg.Write((ushort)code);
            WriteFields(msg);
        }

        public void ReadMessage(NetIncomingMessage msg)
        {
            Debug.Assert(msg.ReadUInt16() == code);
            ReadFields(msg);
            OnReceived();
        }

        public static int PeekCode(NetIncomingMessage msg)
        {
            return msg.PeekUInt16();
        }

        protected abstract void WriteFields(NetOutgoingMessage msg);
        protected abstract void ReadFields(NetIncomingMessage msg);

        #endregion

        #region privates

        private int code;

        #endregion
    }
}
