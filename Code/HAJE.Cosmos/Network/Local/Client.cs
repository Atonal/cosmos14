﻿using Lidgren.Network;
using System;

namespace HAJE.Cosmos.Network.Local
{
    public class Client : IUpdatable
    {
        public Client()
        {
            NetPeerConfiguration config = new NetPeerConfiguration(NetworkInfo.LocalAppName);
            config.AutoFlushSendQueue = true;
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            client = new NetClient(config);
        }

        public readonly PacketDispatcher PacketDispatcher = new PacketDispatcher();
        public readonly UpdateDispatcher UpdateDispatcher = new UpdateDispatcher();
        public event Action<string> OnUnhandledMessage = delegate { };

        public void Connect(string hostName)
        {
            client.Start();
            client.Connect(hostName, NetworkInfo.Port);
        }

        public void Update(GameTime gameTime)
        {
            NetIncomingMessage msg;
            while ((msg = client.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.ErrorMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.VerboseDebugMessage:
                        OnUnhandledMessage(msg.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        string reason = msg.ReadString();
                        OnUnhandledMessage("status changed: " + reason);
                        break;
                    case NetIncomingMessageType.Data:
                        PacketDispatcher.HandleMessage(msg);
                        break;
                }
                client.Recycle(msg);
            }

            UpdateDispatcher.Update(gameTime);
        }

        public void SendMessage(string test)
        {
            NetOutgoingMessage msg = client.CreateMessage();
            msg.Write(test);
            client.SendMessage(msg, NetDeliveryMethod.ReliableOrdered);
        }

        public void SendPacket(PacketBase packet)
        {
            NetOutgoingMessage msg = client.CreateMessage();
            packet.WriteMessage(msg);
            client.SendMessage(msg, packet.DeliveryMethod, packet.SequenceChannel);
        }

        NetClient client;
    }
}
