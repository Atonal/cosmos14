﻿
namespace HAJE.Cosmos.Network.Local.Packet.ClientToServer
{
    public enum PacketCode
    {
        SendCommand,
        SyncMousePoint
    }
}
