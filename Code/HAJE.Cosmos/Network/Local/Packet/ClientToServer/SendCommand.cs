﻿using Lidgren.Network;

namespace HAJE.Cosmos.Network.Local.Packet.ClientToServer
{
    public class SendCommand : PacketBase
    {
        public Local.InputCommand Command;

        protected override void WriteFields(NetOutgoingMessage msg)
        {
            msg.Write(Command);
        }

        protected override void ReadFields(NetIncomingMessage msg)
        {
            Command.Type = (Local.InputCommand.CommandType)msg.ReadByte();
            Command.CommandParameter = msg.ReadByte();
        }

        public SendCommand()
            : base((int)PacketCode.SendCommand)
        {
        }
    }
}
