﻿using Lidgren.Network;

namespace HAJE.Cosmos.Network.Local.Packet.ClientToServer
{
    public class SyncMousePoint : PacketBase
    {
        public float X;
        public float Y; 

        protected override void WriteFields(NetOutgoingMessage msg)
        {
            msg.Write(X);
            msg.Write(Y);
        }

        protected override void ReadFields(NetIncomingMessage msg)
        {
            X = msg.ReadSingle();
            Y = msg.ReadSingle();
        }

        public SyncMousePoint()
            : base((int)PacketCode.SyncMousePoint)
        {
            DeliveryMethod = NetDeliveryMethod.UnreliableSequenced;
            SequenceChannel = (int)Channel.MouseCursor;
        }       
    }
}
