﻿using HAJE.Andromeda.GameBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos
{
    public class Camera
    {
        public const float Far = 10000.0f;
        public const float Near = 1.0f;

        public Camera(Rectangle viewport)
        {
            SetViewport(viewport);
            SetPosition(new Vector2(0, 0));
        }

        public void SetPosition(Vector2 position)
        {
            this.position = position;
            view = Matrix.CreateTranslation(
                -position.X,
                -position.Y,
                0
            );
            Matrix.Multiply(ref view, ref projection, out transform);
        }

        public void SetViewport(Rectangle viewport)
        {
            Radian fieldOfView = Radian.Pi / 4.0f;
            float screenAspect = viewport.Width / viewport.Height;
            Matrix.CreatePerspective(
                fieldOfView,
                screenAspect,
                Near,
                Far,
                out projection
            );
            Matrix.Multiply(ref view, ref projection, out transform);
        }

        public Matrix TransformMatrix
        {
            get
            {
                return transform;
            }
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        #region privates

        Matrix view;
        Matrix projection;
        Matrix transform;
        Vector2 position;

        #endregion
    }
}
