﻿using HAJE.Andromeda.GameBase.Inputs;
using HAJE.Cosmos.Network.Local;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HAJE.Cosmos
{
    /// <summary>
    /// 네트워크를 통해 동기화 된 플레이어 입력 상태
    /// </summary>
    public class PlayerInputState
    {
        public PlayerInputState()
        {
        }

        /// <summary>
        /// 현재 로직 프레임에 특정 키가 눌려있는가 여부를 반환
        /// </summary>
        public bool IsKeyPressed(Keys key)
        {
            return pressedKeys.Contains(key);
        }

        /// <summary>
        /// 현재 로직 프레임에 특정 키가 눌려있지 않는지 여부를 반환
        /// </summary>
        public bool IsKeyReleased(Keys key)
        {
            return !pressedKeys.Contains(key);
        }

        /// <summary>
        /// 현재 로직 프레임에 특정 키가 눌리기 시작했는지 여부를 반환
        /// KeyRelease에서 KeyPressed로 전환되는 특정 한 프레임에만 true이다.
        /// </summary>
        public bool IsKeyDown(Keys key)
        {
            return downKey.Contains(key);
        }

        /// <summary>
        /// 현재 로직 프레임에 특정 키가 떼지기 시작했는지 여부를 반환
        /// KeyPressed에서 KeyReleased로 전환되는 특정 한 프레임에만 true이다.
        /// </summary>
        public bool IsKeyUp(Keys key)
        {
            return upKey.Contains(key);
        }

        /// <summary>
        /// 현재 로직 프레임에 특정 키가 빠르게 두 번 눌렸는지 여부를 반환.
        /// </summary>
        public bool IsKeyDoubleDown(Keys key)
        {
            return doubleDownKey.Contains(key);
        }

        /// <summary>
        /// 다음 로직 프레임으로 이동하기 위해 모든 키의 Down/Up/DoubleDown 상태를 초기화 한다.
        /// </summary>
        public void ClearFrame()
        {
            downKey.Clear();
            doubleDownKey.Clear();
            upKey.Clear();
        }

        public void ApplyCommand(InputCommand inputCommnad)
        {
            switch (inputCommnad.Type)
            {
                case InputCommand.CommandType.KeyboardDouble:
                    doubleDownKey.Add(inputCommnad.KeyboardKey);
                    break;
                case InputCommand.CommandType.KeyboardDown:
                    downKey.Add(inputCommnad.KeyboardKey);
                    pressedKeys.Add(inputCommnad.KeyboardKey);
                    break;
                case InputCommand.CommandType.KeyboardUp:
                    upKey.Add(inputCommnad.KeyboardKey);
                    pressedKeys.Remove(inputCommnad.KeyboardKey);
                    break;
                case InputCommand.CommandType.MouseDouble:
                case InputCommand.CommandType.MouseDown:
                case InputCommand.CommandType.MouseUp:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #region privates

        HashSet<Keys> pressedKeys = new HashSet<Keys>();
        HashSet<Keys> downKey = new HashSet<Keys>();
        HashSet<Keys> doubleDownKey = new HashSet<Keys>();
        HashSet<Keys> upKey = new HashSet<Keys>();

        #endregion
    }
}
