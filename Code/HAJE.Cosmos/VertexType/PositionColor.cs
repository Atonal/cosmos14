﻿using HAJE.Andromeda.GameBase;
using HAJE.Andromeda.GameBase.Graphics;
using DxDeclarationMethod = SharpDX.Direct3D9.DeclarationMethod;
using DxDeclarationType = SharpDX.Direct3D9.DeclarationType;
using DxDeclarationUsage = SharpDX.Direct3D9.DeclarationUsage;
using DxVertexDeclaration = SharpDX.Direct3D9.VertexDeclaration;
using DxVertexElement = SharpDX.Direct3D9.VertexElement;


namespace HAJE.Cosmos.VertexType
{
    public struct PositionColor
    {
        public Vector3 Position;
        public Color Color;

        public static DxVertexDeclaration CreateDeclaration(Device device)
        {
            DxVertexElement[] elements = new[]{
                new DxVertexElement(0, 0, DxDeclarationType.Float3, DxDeclarationMethod.Default, DxDeclarationUsage.Position, 0),
                new DxVertexElement(0, 12, DxDeclarationType.Color, DxDeclarationMethod.Default, DxDeclarationUsage.Color, 0),
                DxVertexElement.VertexDeclarationEnd
            };
            return new DxVertexDeclaration(device.DxDevice, elements);
        }
    }
}

