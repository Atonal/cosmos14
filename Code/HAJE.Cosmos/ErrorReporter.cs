﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace HAJE.Cosmos
{
    public static class ErrorReporter
    {
        public static bool ActiveOnDebugMode = false;

        public static void BeginReport()
        {
#if DEBUG
            if (ActiveOnDebugMode)
#endif
            {
                Application.ThreadException += OnThreadError;
                Thread.GetDomain().UnhandledException += OnUnhandledError;
            }
        }

        private static void OnUnhandledError(object sender, UnhandledExceptionEventArgs e)
        {
            HandleError((Exception)e.ExceptionObject);
        }

        private static void OnThreadError(object sender, ThreadExceptionEventArgs e)
        {
            HandleError(e.Exception);
        }

        private static void HandleError(Exception e)
        {
            MessageBox.Show("처리되지 않은 예외가 발생했습니다.\n게임을 종료합니다.\n\n오류내용 -----------------\n" + e, "오류");
        }
    }
}
